import 'package:flutter/material.dart';
import 'package:flutter_app/pages/home_page.dart';
import 'package:flutter_app/pages/my_page.dart';
import 'package:flutter_app/pages/search_page.dart';
import 'package:flutter_app/pages/stock_page.dart';

class TabNavigator extends StatefulWidget {
  @override
  _TabNavigatorState createState() => _TabNavigatorState();
}

class _TabNavigatorState extends State<TabNavigator> {
  final _defaultColor = Colors.grey;
  final _activeColor = Colors.blue;
  int _currentIndex = 0;
  final PageController _controller = PageController(
    initialPage: 0,
  );
  void _pageChange(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "观奇股票王",
        ),
        centerTitle: true,
      ),
      body: PageView(
        onPageChanged: _pageChange,
        controller: _controller,
        children: [
          HomePage(),
          StockPage(),
          SearchPage(),
          MyPage(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          onTap: (index) {
            _controller.jumpToPage(index);
            setState(() {
              _currentIndex = index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  color: _defaultColor,
                ),
                activeIcon: Icon(
                  Icons.home,
                  color: _activeColor,
                ),
                title: Text(
                  "首页",
                  style: TextStyle(
                    color: _currentIndex == 1 ? _activeColor : _defaultColor,
                  ),
                )),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.camera,
                  color: _defaultColor,
                ),
                activeIcon: Icon(
                  Icons.camera,
                  color: _activeColor,
                ),
                title: Text(
                  "股票",
                  style: TextStyle(
                    color: _currentIndex == 3 ? _activeColor : _defaultColor,
                  ),
                )),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.message,
                  color: _defaultColor,
                ),
                activeIcon: Icon(
                  Icons.message,
                  color: _activeColor,
                ),
                title: Text(
                  "消息",
                  style: TextStyle(
                    color: _currentIndex == 4 ? _activeColor : _defaultColor,
                  ),
                )),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.account_circle,
                  color: _defaultColor,
                ),
                activeIcon: Icon(
                  Icons.account_circle,
                  color: _activeColor,
                ),
                title: Text(
                  "我的",
                  style: TextStyle(
                    color: _currentIndex == 2 ? _activeColor : _defaultColor,
                  ),
                )),
          ]),
    );
  }
}
