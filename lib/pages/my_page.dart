import 'package:flutter/material.dart';
class MyPage extends StatefulWidget {
  @override
  _MyPageState createState() => _MyPageState();
}
const CITY_NAMES=['自选','备选'];
class _MyPageState extends State<MyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child:ListView(
children: _buildList(),
),
      ),
    );
  }
}


List _buildList(){
return CITY_NAMES.map((city)=>_item(city)).toList();
}

Widget _item(String city){
return Container(
height: 80,
margin: EdgeInsets.only(bottom: 5),
alignment: Alignment.center,
decoration: BoxDecoration(color:Colors.teal),
child: Text(
city,
style: TextStyle(color: Colors.white,fontSize: 20),
),
);
}