import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_app/common/webPage.dart';
import 'package:flutter_app/model/news.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    fetchPost().then((News value) {
    showResult = value.result;
  });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          buildGrid(),
        ],
      ),
    );
  }



Widget buildGrid() {
  List<Widget> tiles = []; //先建一个数组用于存放循环生成的widget
  fetchPost().then((News value) {
    showResult = value.result;
    //重新构建刷新
    setState(() {
      
    });
  });
  if(showResult!=null)
  {
  for (var result in showResult) {
    tiles.add(new Column(children: <Widget>[
      Row(crossAxisAlignment: CrossAxisAlignment.start, children: []),
      SizedBox(
        height: 8,
      ),
      ListTile(
        leading: Image.network(result.image),
        title: Text(result.title),
        subtitle: Text(result.passtime),
        onTap: () {
          log(result.path);
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (cx) => MyInfoDetail(
                        title: result.title,
                        url: result.path,
                      )));
        },
      ),
    ]));
  }
  }
  return Column(children: tiles);
}

List<Result> showResult;
Future<News> fetchPost() async {
  final response = await http.get('http://39.97.121.30:9010/OpenApi/GetNews');
  final result = json.decode(response.body);
  return News.fromJson(result);
}






}


