class News {
  final int code;
  final String message;
  final List<Result> result;

  News({this.code, this.message, this.result});

  factory News.fromJson(Map<String, dynamic> json) {
    var list = json["data"] as List;
    List<Result> mylist = list.map((i) => Result.fromJson(i)).toList();
    return News(
      code: json['code'],
      message: json['msg'],
      result: mylist,
    );
  }
}

class Result {
  final String path;
  final String image;
  final String title;
  final String passtime;

  Result({this.path, this.title, this.image, this.passtime});
  factory Result.fromJson(Map<String, dynamic> json) {
    return Result(
      path: json['path'],
      image: json['image'],
      title: json['title'],
      passtime: json['passtime'],
    );
  }
}
